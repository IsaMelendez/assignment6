package linearalgebra;
import java.lang.Math;

/**
 * Vector3d is a class that stores information about a vector with
 * 3 dimensions x, y and z
 * @author Isa Melendez
 * @version 10/2/2023
 */

public class Vector3d{
    private double x;
    private double y;
    private double z;

    /**
     * Constructs the Vector3d object
     * @param x The value of the dimension x
     * @param y The value of the dimension y
     * @param z The value of the dimension z
     */
    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Accesses the private field x
     * @return The value of x
     */
    public double getX(){
        return this.x;
    }

    /**
     * Accesses the private field y
     * @return The value of y
     */
    public double getY(){
        return this.y;
    }

    /**
     * Accesses the private field z
     * @return The value of z
     */
    public double getZ(){
        return this.z;
    }

    /**
     * Calculates the magnitude of the vector
     * @return The final value of the magnitude of the current vector
     */
    public double magnitude(){
        return Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2));
    }

    /**
     * Calculates the dot product of the current vector and another vector
     * @param vector A second vector to calculate the dot product with
     * @return The final value of the dot product of the current vector and the input vector
     */
    public double dotProduct(Vector3d vector){
        return (this.x * vector.getX() + this.y * vector.getY() + this.z * vector.getZ());
    }

    /**
     * Adds 2 vectors together
     * @param vector The vector that we are going to add to the current one
     * @return A new vector that is the result of adding the current one and the one we took as input
     */
    public Vector3d add(Vector3d vector){
        return new Vector3d(this.x + vector.getX(), this.y + vector.getY(), this.z + vector.getZ());
    }
}